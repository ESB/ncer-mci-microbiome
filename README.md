# NCER-MCI-PD microbiome

This repository contains 16S rRNA gene amplicon sequence data analyses related to Parkinson's disease (PD) with and without mild cognitive impairment (MCI) in subjects from the Luxembourg Parkinson’s Study cohort, currently published as a [preprint](https://www.researchsquare.com/article/rs-3372525/v1).

### Workflow

The workflow is set up entirely in R. Required packages should preferably be installed through [renv](https://rstudio.github.io/renv/articles/renv.html) using the lockfile (`renv.lock`) provided in this repository and the `renv::restore()` function; the exact versions of packages used for the publication are the ones in the lockfile. The main workflow, which produces all figures and tables, is performed by knitting the R Markdown document `ncer_mci_report.Rmd`; the resulting pdf report and all other outputs are exported in a directory `../analysis` which should be set up in advance.

### Data availability

The input data for these analyses is not publicly available due to privacy restrictions. They can be requested from NCER-PD (https://www.parkinson.lu/research-participation); requests should be directed to the Data and Sample Access Committee via email: [request.ncer-pd@uni.lu](mailto:request.ncer-pd@uni.lu). For further details regarding the required files, contact the corresponding author.
