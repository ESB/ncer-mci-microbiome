
# Custom boxplot function

box_and_violin <- function(df, varx, vary, cols){
  ggplot(df, aes(x = !!sym(varx), y = !!sym(vary))) +
    geom_violin(aes_string(color = varx), show.legend = FALSE) +
    geom_boxplot(width = 0.3, outlier.shape = 21,
                 aes_string(fill = varx)) +
    scale_fill_manual(values = cols) +
    scale_color_manual(values = cols) +
    theme_classic(base_size = 10) +
    theme(legend.position = "none")}

# Phyloseq object setup

phyloseq_setup <- function(file, df){
  full_df <- read.table(file, sep = "\t", header = TRUE, row.names = 1)
  
  # Set up count table
  counts_mat <- as.matrix(full_df[, grep("mothur",
                                          colnames(full_df),
                                          invert = TRUE)])
  colnames(counts_mat) <- sub("^NCER", "",
                              sub("\\.STLOMN.*", "", colnames(counts_mat)))
    
  # Set up taxonomy
  tax_mat <- full_df[,grep("mothur", colnames(full_df))]
  tax_mat <- tax_mat[,2:ncol(tax_mat)]
  colnames(tax_mat) <- sub("\\.mothur", "", colnames(tax_mat))
  tax_mat <- as.matrix(tax_mat)
  
  # Fill empty spaces with "unclassified"
  tax_mat[tax_mat == ""] <- "unclassified"
  
  # There are also taxa that are very unhelpfully labeled as
  # "metagenome", "gut_metagenomne", "human_gut_metagenome" or even
  # "mouse_gut_metagenome", just call these
  # unclassified as well:
  tax_mat[grep("metagenome", tax_mat)] <- "unclassified"
  
  # For consistency, also replace "unidentified" with "unclassified
  tax_mat[grep("unidentified", tax_mat)] <- "unclassified"
  
  # Fix some problematic taxon names
  tax_mat <- gsub("-", "_", tax_mat)
  
  # Additional cleanup
  
  # Match samples (incl. removing HCMCI from the ASV data)
  counts_mat <- counts_mat[,intersect(colnames(counts_mat),
                                     rownames(df))]
  df <- df[intersect(colnames(counts_mat),
                     rownames(df)),]
  
  # Make phyloseq
  final_phy <- phyloseq(otu_table(counts_mat, taxa_are_rows = TRUE),
                       tax_table(tax_mat),
                       sample_data(df))
}

phyloseq_trim <- function(phylo_obj){
  # Trim zero or singleton taxa
  phylo_obj <- prune_taxa(taxa_sums(phylo_obj) > 1, phylo_obj)
  
  # Trim out taxa present in less than 1/10th of samples
  phylo_obj <- filter_taxa(phylo_obj, function(x)
    sum(x > 0) > round(nsamples(phylo_obj)/10), prune = TRUE)

  # Trim any remaining contaminants/misclassifications that
  # are labeled as Mitochondria or Chloroplasts
  phylo_obj <- subset_taxa(phylo_obj, Order != "Chloroplast" &
                                 Family != "Mitochondria")
  
  # Trim samples with < 10000 reads
  phylo_obj <- prune_samples(sample_sums(phylo_obj) > 10000, phylo_obj)
}

phyloseq_summarize <- function(phylo_obj, levels){

  # Function for collapsing to a specific taxonomic level
  collapseTaxLevel <- function(phylo_obj, level){
    
    levelNum <- grep(level, colnames(tax_table(phylo_obj)))
    
    otu <- cbind(tax_table(phylo_obj)[, levelNum],
                 as.data.frame(as(otu_table(phylo_obj), "matrix")))
    otu <- melt(otu, id = level)
    otu <- acast(otu, as.formula(paste(level, "~variable", sep = "")),  sum)
    
    tax <- unique(as(tax_table(phylo_obj), "matrix")[,1:levelNum])
    rownames(tax) <- unname(tax[,levelNum])
    
    # Combine unclassifieds at the chosen level into
    # one "unclassified" bin (if there are any)
    if(length(grep("unclassified", tax[,level]) > 0)){
      tax <- tax[-grep("unclassified", tax[,level]),]
      tax <- rbind(tax, unclassified = rep("unclassified", ncol(tax)))
    }
    
    tax <- tax[order(rownames(tax)),]
    
    new_phylo_obj <- phyloseq(otu_table(otu, taxa_are_rows = TRUE),
                              sample_data(phylo_obj),
                              tax_table(tax))
    return(new_phylo_obj)
  }
  
  # Phyloseq objects for each level:
  phyloseqs_list <- c(
    lapply(levels, function(x)
      collapseTaxLevel(phylo_obj, x)),
    phylo_obj)
  names(phyloseqs_list) <- c(levels, "ASV")
  
  return(phyloseqs_list)
}

adiv_comp <- function(adiv_df, vars, div_vars){
  
  # Calculate correlations for numeric variables
  if(length(vars$num_vars) > 0){
    div_cors_num <- do.call("rbind", sapply(vars$num_vars, function(x){
      lapply(div_vars, function(div){
        res <- cor.test(adiv_df[,x], adiv_df[,div], method = "pearson")
        data.frame(Variable = x,
                   Index = div,
                   Statistic = res$statistic,
                   p = res$p.value,
                   r = res$estimate,
                   ci = paste(round(res$conf.int, digits = 3), collapse = ", "),
                   Test = "Pearson")
      })
    }))
  }
  
  # Calculate wilcoxon or kruskal for categorical variables
  div_comp_cat <- do.call(
    "rbind", sapply(vars$cat_vars, function(x){
      if(length(levels(adiv_df[[x]])) == 2){
        lapply(div_vars, function(div){
          res <- wilcox.test(as.formula(paste(div, "~", x)), adiv_df)
          data.frame(
            Variable = x,
            Index = div,
            Statistic = res$statistic,
            p = res$p.value,
            Test = "Wilcoxon")})
      } else {
        lapply(div_vars, function(div){
          res <- kruskal.test(as.formula(paste(div, "~", x)), adiv_df)
          data.frame(
            Variable = x,
            Index = div,
            Statistic = res$statistic,
            p = res$p.value,
            Test = "Kruskal")})
      }}))
  
  # Combine
  if(length(vars$num_vars) > 0){
    div_comp <- merge(div_cors_num, div_comp_cat,
                      all.x = TRUE, all.y = TRUE)
  } else {
    div_comp <- div_comp_cat
  }
  
  return(div_comp)
}

# Adonis for beta diversity: individual variables
bdiv_ad_single <- function(bdiv_list, vars){

  test_vars <- c(vars$num_vars, vars$cat_vars)
  phylo_obj <- bdiv_list[["phy"]]
  
  ad_res <- sapply(test_vars, function(x){

    meta_df <- as(sample_data(phylo_obj), "data.frame")
    meta_df <- meta_df[!is.na(meta_df[,x]),]
    
    count_df <-  as.data.frame(t(as(otu_table(phylo_obj), "matrix")))
    count_df <- count_df[rownames(meta_df),]
    
    dist <- vegdist(count_df, method = "bray")
    
    unlist(adonis2(as.formula(paste("dist ~", x)),
                       data = meta_df,
                       perm = 9999)[x,])
    })
  
  t(ad_res)
}

# Adonis for beta diversity: combined model for all samples
bdiv_ad_combo <- function(phylo_obj, conf_vars){

  dist <- vegdist(
    as.data.frame(t(as(otu_table(phylo_obj), "matrix"))),
    method = "bray")
  
  adonis2(as.formula(paste("dist ~ Group +",
                           paste(conf_vars, collapse = "+"))),
          data = as(sample_data(phylo_obj), "data.frame"),
          perm = 9999, by = "margin")
}

# Adonis for beta diversity: combined reduced model
bdiv_ad_combo_trim <- function(phylo_obj){
  
  dist <- vegdist(
    as.data.frame(t(as(otu_table(phylo_obj), "matrix"))),
    method = "bray")
  
  adonis2(dist ~ Group + Sex + BMI_scaled + Constipation,
          data = as(sample_data(phylo_obj), "data.frame"),
          perm = 9999, by = "margin")
}

# Adonis for beta diversity: combined model for PD-only
bdiv_ad_combo_pd <- function(phylo_obj){
  
  dist <- vegdist(
    as.data.frame(t(as(otu_table(phylo_obj), "matrix"))),
    method = "bray")
  
  adonis2(dist ~ MCI + Sex + BMI_scaled +
                   Constipation + LEDD_mg_per_day +
            Disease_duration_since_diagnosis_years,
                 data = as(sample_data(phylo_obj), "data.frame"),
                 perm = 9999, by = "margin")
}

# Prettier adonis output
ad_res_reorg <- function(ad_res, labels){
  ad_res %>%
    tidy() %>%
    mutate(
      p.value = scales::pvalue(p.value),
      term = labels[term]
    )
}

# DESeq2 for the main contrast
ds2_main <- function(phylo_list, conf_vars){

  ds2conf <- function(phylo_obj){
    ds2 <- phyloseq_to_deseq2(phylo_obj,
                              as.formula(paste("~ Group +",
                                               paste(conf_vars, collapse = "+"))))
    ds2 <- DESeq(ds2, sfType = "poscounts")
    
    full_res <- do.call(
      "rbind", lapply(
        data.frame(combn(levels(sample_data(phylo_obj)$Group), 2)),
        function(x) data.frame(results(ds2,
                                       contrast = c("Group", x[2], x[1]),
                                       alpha = 0.05),
                               Taxon = rownames(ds2),
                               Contrast = paste(x[2], x[1], sep = "."))))
    rownames(full_res) <- NULL
    
    return(full_res)
  }
  
  ds2_res <- do.call("rbind",
                     lapply(names(phylo_list), function(x)
                       data.frame(ds2conf(phylo_list[[x]]),
                                  Level = x)))
}

# Reorganising ANCOM-BC results
abc_reorg <- function(res_obj, res_type, lvls, grpvar){
  res <- do.call("rbind", lapply(1:length(lvls), function(x){
    df <- res_obj[[x]][[res_type]]
    group_lvls <- sub("diff_", "", grep(paste0("diff_", grpvar), colnames(df), value = TRUE))
    
    data.frame(taxon = df$taxon,
               do.call("cbind",
                       lapply(group_lvls, function(grp)
                         df[,paste(c("lfc", "se", "W", "p", "q"),
                                   grp, sep = "_")])),
               Level = lvls[x])
  }))
  res_m <- melt(res)
  res_m$stat <- factor(sub("_.*", "", res_m$variable))
  res_m$Contrast <- gsub(grpvar, "",
                                sub("W_", "",
                                    (sub(".*[a-z]_", "", res_m$variable))))
  res_m$variable <- NULL
  res_m$Level <- factor(res_m$Level, levels = lvls)
  dcast(res_m, taxon + Contrast + Level ~ stat, value.var = "value")
}

custom_datest <- function(phy_obj, conf_vars){
      summary(
        testDA(phy_obj,
               predictor = "MCI",
               covars = conf_vars,
               R = 50,
               verbose = FALSE))
}

datest_lic <- function(phylist, conf_vars){
  do.call("rbind", lapply(names(phylist), function(x){
    
      res <- rbind(
        DA.lic(phylist[[x]],
               predictor = "Group",
               covars = conf_vars,
               out.all = FALSE,
               coeff = 2),
        DA.lic(phylist[[x]],
               predictor = "Group",
               covars = conf_vars,
               out.all = FALSE,
               coeff = 3),
        DA.lic(subset_samples(phylist[[x]], Group != "Control"),
               predictor = "MCI",
               covars = conf_vars))
    res$Level <- x
    res[,c(colnames(res)[1:grep("Method", colnames(res))], "Level")]
  }
  ))
}
