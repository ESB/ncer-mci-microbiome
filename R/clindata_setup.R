## Clinical data setup and export

# Run only once prior to analyses to set up the specific
# clinical data file for this analysis.

# Read in clinical data
clin_df <- read.csv("../../data/2021-09-17_MCI_BIOME_clin_merged.csv",
                    stringsAsFactors = TRUE)

# Exclude HC-MCI as this group is analysed elsewhere
clin_df <- subset(clin_df, Group != "HCMCI")

# Set up NC vs MCI variable
clin_df$MCI <- factor(sub("PD", "", sub("HC", "", clin_df$Group)),
                      levels = c("NC", "MCI"))

# Education: split by median
clin_df$Education_categorical <- factor(clin_df$Years_of_education < 
                                          median(clin_df$Years_of_education, na.rm = TRUE),
                                        levels = c(TRUE, FALSE))
levels(clin_df$Education_categorical) <- c("less_than_14", "14_or_more")

# Scaled age variable
clin_df$Age_scaled <- as.vector(scale(clin_df$Age))

# Married vs not
clin_df$Married <- factor(clin_df$Maritial_status %in% c("Married", "Domestic partnership"))
levels(clin_df$Married) <- c("no", "yes")
# Fix NA values
clin_df[is.na(clin_df$Maritial_status), "Married"] <- NA 

# BMI categorizations (based on WHO Europe classifications)
clin_df$BMI_categorical <- cut(clin_df$BMI,
                               breaks = c(0, 18.5, 24.9, 29.9, 34.9, 39.9, 50))
clin_df$BMI_obese <- factor(clin_df$BMI > 29.9)
levels(clin_df$BMI_obese) <- c("no", "yes")

# Set rownames
rownames(clin_df) <- gsub("-", ".", clin_df$StoolKitID)

# Export
write.csv(clin_df,
          "../../data/2022-01-31_MCI-BIOME_PD_clin.csv",
          row.names = TRUE)
